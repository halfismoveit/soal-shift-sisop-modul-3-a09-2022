# soal-shift-sisop-modul-3-A09-2022

## Soal 1
### Deskripsi Soal
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

#### Solusi
##### a
Pertama, kita akan mendeklarasikan library-library yang akan digunakan serta variabel dan fungsi yang akan digunakan.
```
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <dirent.h>

pthread_t thread[10];
pid_t child;
FILE *music_file;
FILE *quote_file;
char *password = "mihinomenestakmal";

void *unzip_or_create_dir(void *args);
char* base64Decoder(char encoded[], int len_str);
void *decoding(void *args);
void *zip(void *args);
void *unzip_or_create_file(void *args)
void *final_combine(void *args) 
```
Kemudian, kita mengunduh dua file, yakni quote.zip dan music.zip dari Google Drive yang sudah disediakan. Proses mengunduh akan menggunakan "wget". Kemudian, kedua file zip akan dibuka zipnya serta membuat sebuah direktori bernama "hasil". Karena kita tidak diperbolehkan menggunakan fungsi seperti "mkdir", "system", dan sebagainya, kita akan menggunakan "exec".

Fungsi utama:
```
int main() {
    //zip & create dir
    for(int i = 0; i <= 4; i++) {
        pthread_create(&thread[i], NULL, &unzip_or_create_dir, NULL);
    }

    for(int i = 0; i < 3; i++) {
        pthread_join(thread[i], NULL);
    }

    sleep(1);

    ...
```
Fungsi untuk unzip dan membuat direktori:
```
void *unzip_or_create_dir(void *args) {
    char *argv1[] = {"wget", "-O", "/home/hapis/Downloads/quote.zip", "https://drive.google.com/u/0/uc?id=1LYFd6FLDgpIBPqbG6q-2fySinmy43GcD&export=download", "-qq", NULL};
    char *argv2[] = {"wget", "-O", "/home/hapis/Downloads/music.zip", "https://drive.google.com/u/0/uc?id=1FzbaO07c7LImW-EJSD5ETVqrktZOiRmO&export=download", "-qq", NULL};
    char *argv3[] = {"unzip", "-o", "Downloads/music.zip", "-d", "music", NULL};
    char *argv4[] = {"unzip", "-o", "Downloads/quote.zip", "-d", "quote", NULL};
    char *argv5[] = {"mkdir", "-p", "hasil", NULL};


    pthread_t id = pthread_self();
    int status;
    
    if(pthread_equal(id, thread[0])) {
        child = fork();

        if(child == 0) {
            execv("wget", argv1);
        }
    }
    
    if(pthread_equal(id, thread[1])) {
        child = fork();

        if(child == 0) {
            execv("wget", argv2);
        }
    }
    
    if(pthread_equal(id, thread[2])) {
        child = fork();

        if(child == 0) {
            execv("/bin/unzip", argv3);
        }
    }

    if(pthread_equal(id, thread[3])) {
        child = fork();

        if(child == 0) {
            execv("/bin/unzip", argv4);
        }
    }

    if(pthread_equal(id, thread[4])) {
        child = fork();

        if(child == 0) {
            execv("/bin/mkdir", argv5);
        }
    }
}
```
##### b & c
Kemudian, akan dilakukan decode pada file-file ".txt" di folder quote dan music serta membuat daftar isi dari quote dan music yang sudah di-decode. Lalu, hasil decode akan dipindahkan ke dalam folder "hasil" yang sudah dibuat sebelumya.

Fungsi utama:
```
    ...

    //handling .txt
    for(int i = 3; i < 5; i++) {
        pthread_create(&thread[i], NULL, &decoding, NULL);
    }

    for(int i = 3; i < 5; i++) {
        pthread_join(thread[i], NULL);
    }

    sleep(1); 

    ...
```
Fungsi untuk decode:
```
// Bagian decoding
char* base64Decoder(char encoded[], int len_str) {
    char* decoded_string;
 
    decoded_string = (char*)malloc(sizeof(char) * 100);
 
    int i, j, k = 0;
 
    int num = 0;
 
    int count_bits = 0;
 
    for (i = 0; i < len_str; i += 4) {
        num = 0, count_bits = 0;
        for (j = 0; j < 4; j++)
        {
             
            // make space for 6 bits.
            if (encoded[i + j] != '=')
            {
                num = num << 6;
                count_bits += 6;
            }
 
            if (encoded[i + j] >= 'A' && encoded[i + j] <= 'Z')
                num = num | (encoded[i + j] - 'A');
 
            else if (encoded[i + j] >= 'a' && encoded[i + j] <= 'z')
                num = num | (encoded[i + j] - 'a' + 26);
 
            else if (encoded[i + j] >= '0' && encoded[i + j] <= '9')
                num = num | (encoded[i + j] - '0' + 52);
 
            else if (encoded[i + j] == '+')
                num = num | 62;
 
            else if (encoded[i + j] == '/')
                num = num | 63;
 
            else {
                num = num >> 2;
                count_bits -= 2;
            }
        }
 
        while (count_bits != 0) {
            count_bits -= 8;
            decoded_string[k++] = (num >> count_bits) & 255;
        }
    }
 
    decoded_string[k] = '\0';
 
    return decoded_string;
}
// End bagian decoding

void *decoding(void *args) {
    pthread_t id = pthread_self();

    if(pthread_equal(id, thread[3])) {
        music_file = fopen("hasil/music.txt", "w");

        DIR *d;
        struct dirent *dir;
        
        d = opendir("music");
        if (d) {
            while ((dir = readdir(d)) != NULL) {
                if(strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0) {
                    char *filename = malloc(256);
                    sprintf(filename, "music/%s", dir->d_name);

                    FILE *fp = fopen(filename, "r");

                    if(fp == NULL) {
                        printf("Error reading file\n");
                    }

                    const unsigned MAX_LENGTH = 256;
                    char buffer[MAX_LENGTH];

                    while(fgets(buffer, MAX_LENGTH, fp)) {
                        fscanf(fp, "%s\n", buffer);

                        int len = strlen(buffer);

                        fprintf(music_file, "%s\n", base64Decoder(buffer, len));
                    }
                    fclose(fp);
                }
            }

            closedir(d);
        }

        fclose(music_file);
    }

    if(pthread_equal(id, thread[4])) {
        quote_file = fopen("hasil/quote.txt", "w");

        DIR *d;
        struct dirent *dir;
        
        d = opendir("quote");
        if (d) {
            while ((dir = readdir(d)) != NULL) {
               if(strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0) {
                    char *filename = malloc(256);
                    sprintf(filename, "quote/%s", dir->d_name);

                    FILE *fp = fopen(filename, "r");

                    if(fp == NULL) {
                        printf("Error reading file\n");
                    }

                    const unsigned MAX_LENGTH = 256;
                    char buffer[MAX_LENGTH];

                    while(fgets(buffer, MAX_LENGTH, fp)) {
                        fscanf(fp, "%s\n", buffer);

                        int len = strlen(buffer);

                        fprintf(quote_file, "%s\n", base64Decoder(buffer, len));
                    }
                    fclose(fp);
                }
            }

            closedir(d);
        }

        fclose(quote_file);
    }

    return NULL;
}
```
##### d
Lalu, folder hasil akan di-zip dan diberi password "minihomenest[user]" dalam hal ini "minihomenestakmal".

Fungsi utama:
```
    ... 

    //zip
    for(int i = 5; i < 6; i++) {
        pthread_create(&thread[i], NULL, &zip, NULL);
        pthread_join(thread[i], NULL);
    }

    sleep(1);

    ...
```
Fungsi zip:
```
void *zip(void *args) {
    pthread_t id = pthread_self();
    char *argv[] = {"zip", "-P", password, "-r", "hasil.zip", "hasil", NULL};

    if(pthread_self() == thread[5]) {
        child = fork();

        if(child == 0) {
            execv("/bin/zip", argv);
        }
    }
}
```

##### e
Lalu, file "hasil.zip" akan di-unzip. Setelah itu, akan dimasukkan file "no.txt" yang berisi kata "No" ke dalamnya. Setelah itu, "hasil.zip" dan "no.txt" yang sudah digabung akan di-zip kembali dengan password yang sama. Sehingga, hasil akhirnya adalah file "hasil.zip" yang berisi sebuah folder "hasil" dan sebuah file "no.txt".

Fungsi utama:
```
    ...

    // unzip or create file
    for(int i = 6; i < 8; i++) {
        pthread_create(&thread[i], NULL, &unzip_or_create_file, NULL);
    }

    for(int i = 6; i < 8; i++) {
        pthread_join(thread[i], NULL);
    }

    sleep(1);

    // final combine
    for(int i = 8; i < 9; i++) {
        pthread_create(&thread[i], NULL, &final_combine, NULL);
        pthread_join(thread[i], NULL);
    }
    
    pthread_exit(thread);
    exit(0);
    return 0;
}
```
Fungsi untuk unzip, menambahkan file "no.txt", dan meng-zip kembali.
```
void *unzip_or_create_file(void *args) {
    pthread_t id = pthread_self();
    char *argv[] = {"unzip", "-o", "-P", password, "hasil.zip", NULL};

    if(pthread_equal(id, thread[6])) {

        child = fork();

        if(child == 0) {
            execv("/bin/unzip", argv);
        }
    }

    if(pthread_equal(id, thread[7])) {

        FILE *fp = fopen("no.txt", "w");
        char *str = "No";

        fprintf(fp,"%s", str);

        fclose(fp);
    }
}

void *final_combine(void *args) {
    if(pthread_equal(pthread_self(), thread[8])) {

        child = fork();

        if(child == 0) {
            char *argv[] = {"zip", "-P", password, "hasil.zip", "hasil", "no.txt", NULL};
            execv("/bin/zip", argv);
        }
    }
}
```
##### Kendala

## Soal 2
### Deskripsi Soal
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:
#### a
Akan dibuat file "users.txt" yang berisi informasi pengguna yakni nama user dan passwordnya. Dari client akan disediakan dua opsi untuk register atau login. Untuk register, data baru akan dimasukkan ke users.txt. Untuk login, akan dicocokkan data username dan passwordnya dengan data di "users.txt". Dalam melakukan register, ada pula ketentuan seperti username tidak boleh sama dengan yang sudah ada, password minimal 6 karakter panjangnya, alphanumeric, dan kombinasi huruf besar dan kecil.
#### b
Sistem memiliki database untuk menampung problem-problem yang bernama "problem.tsv".
#### c
Client yang telah login dapat menggunakan command "add" untuk menambahkan problem yang dilengkapi dengan: judul, deskripsi, file input, dan file output. Untuk file output nantinya akan dijadikan kunci jawaban untuk menyelesaikan problemnya.
#### d
Client yang telah login dapat menggunakan command "see" untuk melihat problem-problem yang ada.
#### e
Client yang telah login dapat menggunakan command "download <judul problem>" untuk mengunduh problem.
#### f
Client yang telah login dapat menggunakan command "‘submit <judul-problem> <path-file-output.txt>" untuk mengumpulkan jawaban problem yang nantinya akan dicocokkan dengan kunci jawaban.
#### g
Server dapat menangani multiple connection.

## Soal 3
### Deskripsi Soal
Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.
#### Solusi
##### a
Pertama, kita akan mendeklarasikan library, variabel, dan fungsi apa saja yang akan digunakan.
```
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <syslog.h>
#include <limits.h>

pthread_t tid[200];
int flag = 1;

typedef struct struct_path{
    char from[PATH_MAX];
    char cwd[PATH_MAX];
} struct_path;

void createDir(void *param);
void toLower(char *param);
int checkFile(const char *path);
void moveFile(char *p_from, char *p_cwd);
void *move(void *s_path);
void sortFile(char *from);
void extract(char *file);
```
Kemudian, akan dilakukan ekstrak isi file "hartakarun.zip" yang sudah ada di dalam folder "/home/[user]/shift3". Lalu, hasil ekstraknya akan dimasukkan ke dalam folder "hartakarun" yang telah ada. Setelah itu, secara rekursif menggunakan "while", akan diklasifikasikan file-file di dalam folder "hartakarun" berdasarkan ekstensi filenya.

Fungsi main:
```
int main() {
    struct_path s_path;
    getcwd(s_path.cwd, sizeof(s_path.cwd));

    strcpy(s_path.from, "/home/hapis/shift3/hartakarun");
    extract("unzip /home/hapis/shift3/hartakarun.zip");
    sortFile(s_path.from);

    return 0;
}
```
Fungsi untuk mengekstrak file "hartakarun.zip":
```
// extract zip file
void extract(char *value) 
{
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer,'\0',sizeof(buffer));

    file = popen(value ,"r");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n",buffer);
        pclose(file);
    }
}
```
Fungsi untuk mengklasifikasikan file:
```
// sorting file by category
void sortFile(char *from)
{
    struct_path s_path;
    struct dirent *dp;
    DIR *dir;

    flag = 1;
    int index = 0;
    strcpy(s_path.cwd, "/home/hapis/shift3/hartakarun");

    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);

            pthread_create(&tid[index], NULL, move, (void *)&s_path);
            sleep(1);
            index++;
        }
    }

    if(!flag){
        printf("Program failed\n");
    }else {
        printf("Program success\n");
    }
}
```
##### b & c
Dalam mengklasifikasikan file, jika ditemukan file atau folder tanpa ekstensi, akan dimasukkan ke dalam folder "unknown". Lalu, jika file tersebut hidden, akan dimasukkan ke dalam folder "hidden". Strtok dapat digunakan dalam mengklasifikasikan file atau folder karena dengan menggunakan strtok yang membagi nama file atau folder menjadi token-token, kita dapat mengetahui ektensi dari suatu file atau folder yang ditandai dengan tanda titik ".".

Fungsi untuk mengklasifikasikan file atau folder: 
```
// move
void *move(void *s_path)
{
    struct_path s_paths = *(struct_path*) s_path;
    moveFile(s_paths.from, s_paths.cwd);
    pthread_exit(0);
}

// move file
void moveFile(char *p_from, char *p_cwd)
{
    char file_name[300], file_ext[30], temp[300];
    char *buffer;

    strcpy(temp, p_from);
    buffer = strtok(temp, "/");

    while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }

    toLower(file_ext);

    // new dest
    strcat(p_cwd, "/");
    strcat(p_cwd, file_ext);
    strcat(p_cwd, "/");
    strcat(p_cwd, file_name);

    // information
    printf("from %s: \nfile_name: %s\nfile_ext: %s\ndest: %s\n\n", p_from, file_name, file_ext, p_cwd);

    // create dir and move file
    createDir(file_ext);
    rename(p_from, p_cwd);
}

// convert upper to lower
void toLower(char *param)
{
    int i = 0;
    for (i; param[i]; i++){
        param[i] = tolower(param[i]);
    }
}
```
##### d & e
Untuk d dan e, folder "hartakarun" yang berisi klasifikasi file akan dikirim oleh client ke server dengan menggunakan command "send hartakarun.zip"
##### Kendala
1. File "star._" berhasil diidentifikasi oleh program dalam konteks folder untuk pengklasifikasiannya. Akan tetapi, pada hasil akhir, file "star._" tidak diklasifikasikan.
2. Client, server, dan mekanisme pengiriman "hartakarun.zip" belum selesai.
